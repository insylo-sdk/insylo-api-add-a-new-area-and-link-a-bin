# Insylo API Demo Example

You can learn how Insylo API works

## Getting Started

### Prerequisites

* Python3.7
* Pip

### Run following commands to make it run.
* `pip install -r requirements.txt`
* `python main.py`
