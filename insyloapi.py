#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 14 15:18:16 2020

@author: draba
"""
import requests
import json


class InsyloAPI(object):

    def __init__(self, username, password):
        """Retrieve a new token from AP
        """
        self.api_host = "https://apis.insylo.io"
        url = self.api_host + '/v2/login/'
        data = json.dumps({
                'username': username,
                "password": password
            }
        )
        response = requests.post(
            url, data=data,
            headers={"Content-Type": "application/json",
            }
        )
        
        if response.status_code == 200:
            self.token = response.json().get("token")
        else:
            self.token = None            

    def get_areas(self):
        """ Gives the Areas assigned to the user
        """
        url = self.api_host + "/v2/areas/"
        response = requests.get(
            url, headers={"Content-Type": "application/json",
                "Authorization": "Bearer " + self.token
            }
        )
        return response.json()


    def get_bins(self, area_id):
        """Gives the list of Bins assigned to the user
        """
        url = self.api_host + "/v2/bins/?areaId={}&limit=30&offset=0".format(area_id)
        response = requests.get(
            url, headers={"Content-Type": "application/json",
                "Authorization": "Bearer " + self.token
            }
        )
        return response.json()

    def get_metrics(self, binuuid, sdate=None, edate=None):
        """Gives the timeseries of the Bin
        """        
        url = ''
        if sdate == None or edate == None:
            url = self.api_host + "/v2/bins/{}/device/?limit=800&sort=-1".format(binuuid)
        else:
            # build the query string params
            sdate_str = sdate.strftime("%Y-%m-%dT%H:%M:%S.000Z")
            edate_str = edate.strftime("%Y-%m-%dT%H:%M:%S.000Z")
            url = self.api_host + "/v2/bins/{}/device/?from_date={}&to_date={}&limit=800&sort=-1".format(binuuid, sdate_str, edate_str)
        response = requests.get(
            url, headers={"Content-Type": "application/json",
                "Authorization": "Bearer " + self.token
            }
        )
        return response.json()

    def get_diets(self):
        """Gives the set of available diets
        """        
        url = self.api_host + "/v2/recipes/"
        response = requests.get(
            url, headers={"Content-Type": "application/json",
                "Authorization": "Bearer " + self.token
            }
        )
        return response.json()

    def set_diet(self, datapointId, payload):
        """Set a specific diet for a given datapoint
        """
        url = ''
        url = self.api_host + "/v2/device/{}/".format(datapointId)
        response = requests.put(
            url, data=json.dumps(payload), headers={"Content-Type": "application/json",
                "Authorization": "Bearer " + self.token
            }
        )
        return response.json()

    def create_user(self, payload):
        """Create a user
        """
        url = ''
        url = self.api_host + "/v2/accounts/users/"
        response = requests.post(
            url, data=json.dumps(payload), headers={"Content-Type": "application/json",
                "Authorization": "Bearer " + self.token
            }
        )
        return response.json()

    def get_user(self, user):
        """Get user data
        """
        url = ''
        url = self.api_host + "/v2/accounts/users/{}/".format(user)
        response = requests.get(
            url, headers={"Content-Type": "application/json",
                "Authorization": "Bearer " + self.token
            }
        )
        return response.json()

    def create_area(self, payload):
        """Create an area
        """
        url = ''
        url = self.api_host + "/v2/areas/"
        response = requests.post(
            url, data=json.dumps(payload), headers={"Content-Type": "application/json",
                "Authorization": "Bearer " + self.token
            }
        )
        return response.json()

    def create_user_group(self, payload):
        """Create a usergroup
        """
        url = ''
        url = self.api_host + "/v2/accounts/usergroups/"
        response = requests.post(
            url, data=json.dumps(payload), headers={"Content-Type": "application/json",
                "Authorization": "Bearer " + self.token
            }
        )
        return response.json()
    
    def update_bin(self, binUUID, payload):
        """Update Bin data
        """
        url = ''
        url = self.api_host + "/v2/bins/{}/".format(binUUID)
        response = requests.put(
            url, data=json.dumps(payload), headers={"Content-Type": "application/json",
                "Authorization": "Bearer " + self.token
            }
        )
        return response.json()
    
    #WARNING STIL NOT AVAILABLE
    def add_area_permission(self, payload):
        """Create AreaPermission
        """
        url = ''
        url = self.api_host + "/v2/accounts/areapwermission/"
        response = requests.post(
            url, data=json.dumps(payload), headers={"Content-Type": "application/json",
                "Authorization": "Bearer " + self.token
            }
        )
        return response.json()
    