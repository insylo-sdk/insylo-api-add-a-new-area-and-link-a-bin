#  ____                        _           _    ____ ___
# / ___|  __ _ _ __ ___  _ __ | | ___     / \  |  _ \_ _|
# \___ \ / _` | '_ ` _ \| '_ \| |/ _ \   / _ \ | |_) | |   
#  ___) | (_| | | | | | | |_) | |  __/  / ___ \|  __/| |  
# |____/ \__,_|_| |_| |_| .__/|_|\___| /_/   \_\_|  |___|
#                       |_|

import pandas as pd
import datetime
import json
from pandas.plotting import register_matplotlib_converters
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from insyloapi import InsyloAPI

register_matplotlib_converters()

# Main client - full granted user
username = "USER"
password = "PASSWORD"

if __name__ == "__main__":
    # Connect to the API and retrieve a new token 
    insylo = InsyloAPI(username, password)
    getParentCreds = insylo.get_user(username)
    
    ownerId = getParentCreds["id"]

    # New user data
    newUserData = {
      "email": "newuser@corp.com",
      "firstName": "Name",
      "lastName": "Name",
      "parentId": ownerId,    # Granted user admin
      "password": "111222333",
      "staff": 0,
      "status": 1,
      "username": "newuser@corp.com"
    }

    # Create a new user
    newUserResponse = insylo.create_user(newUserData)
    
    #############################
    # Assing a user into a group
    #############################    
    # The given group defines a set of permissions the user have. Current groups:   
    # "5d518bfac8c79e0e709c9d8a" - "Admin"    
    # "5d5d699ec8c79e0e70a198fe" - "Client"
    # "5d839077b04521d2b2f929b6" - "ReadOnlyClientCanChangeDensity" 
    # "5da5b97ae67a58ff461ebaa3" - "ReadOnlyClient"
    # "5e16be4555ef13740dce582c" - "ReadOnlyClientWithoutRecipe"  
    newUserGroupData = {
            "groups": [ "5d5d699ec8c79e0e70a198fe" ], 
            "userId": newUserResponse['insertedId'] 
            }
    
    # Assign a use into a predefined group
    newUserGroupResponse = insylo.create_user_group(newUserGroupData)

    # Create a new area
    areaData = {
          "contactName": "Name",
          "contactNumber": "555-123456",
          "description": "Short name",
          "label": "Code location",
          "location": {
            "coordinates": {
              "latitude": 41.7282,
              "longitude": 1.99282
            }
          },
          "ownerId": ownerId # Granted user admin
        }
            
    areaDataResponse = insylo.create_area(areaData)

    # Move an already created bin into this areas
    binToMove = "BIN1AF96675-7B5E-4EA1-B7C9-E170B3858111"
    insylo.update_bin(binToMove, {"areaId":areaDataResponse['insertedId'], "ownerId":ownerId})
        
    #############################################
    # Grant the new created user to this area
    grantedAreas = [areaDataResponse['insertedId']]
    for area in grantedAreas:
        # WARNING!!!! ENDPOINT STILL INDER DEVELOPMENT
        insylo.add_area_permission({"areaId":area, "userId":newUserGroupResponse})

    ###########################################################################
    # Log in with this new user
    insylo = InsyloAPI(newUserData['username'], newUserData['password'])
    areas = insylo.get_areas()
    binUUID = ""
    # Get bins from each area
    for area in areas["data"][0:1]:
        bins = insylo.get_bins(area["id"])
        try:
            # Here we just print some information from area -> bin relationship.
            for bi in bins["data"][0:1]:
                print("{} :: {} :: {} :: {}".format(bi['uuid'], area['label'],area['description'], bi['label']))

                # Work with an specific BIN                
                binUUID = bi['uuid']
                
                # Set a data time window to work with
                end_date_d = datetime.datetime(2020, 1, 27, 20, 00, 00)
                start_date_d = end_date_d + datetime.timedelta(days=-5)

                # Get reading from a given bin and a time window
                readings = insylo.get_metrics(binUUID, start_date_d, end_date_d)
                
                # Get all readings from the previous N days
                weights = [[sample['id'], 
                            sample['timestamp']['createdAt'],
                            float(sample['weight'])] for sample in readings['data']]
                
                #Create DataFrame to plot timeserie
                data = pd.DataFrame(weights, columns=['id', 'ts', 'weight'])
                data['ts'] = pd.to_datetime(data['ts'])
                data.set_index('ts')
            
                ##############################################
                # Detect refillings greated that maxLoad tonnes
                # assuming hourly readings, direct substraction between 
                # current and previous data, give us a material variation.  
                maxLoad = 1.0                    
                # Every variation larger than maxLoad will be accepted as a refilling
                data['lag_1'] = data.weight.shift(-1)
                data['lag_1'] = data.lag_1.fillna(method='backfill')
                data['diff']  = data.weight - data.lag_1
                isLoad = data['diff']>maxLoad
                data['stock']  = data[isLoad]['weight']
                data.sort_index(ascending=False, inplace=True)
                        
                ##############################################
                # Plot time series to validate refilling detection
                fig = plt.figure(constrained_layout=True,figsize=(8,6), dpi=100)
                ax = fig.add_subplot(111)
                ax.plot(data['ts'], data['weight'] , 'k', alpha=0.5)
                ax.plot(data['ts'], data['stock'] , 'r+', alpha=1)
                ax.set_xlabel('Date')
                ax.set_ylabel('Bin stock (Tn)')
                #set major ticks format
                ax.xaxis.set_major_formatter(mdates.DateFormatter('%b %d'))   
                plt.show()
                plt.close()

        except:
            print('No bins')
            